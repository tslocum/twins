module gitlab.com/tslocum/twins

go 1.15

require (
	github.com/h2non/filetype v1.1.1
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/yookoala/gofast v0.6.0
	gitlab.com/tslocum/gmitohtml v1.0.3
	golang.org/x/tools v0.1.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
