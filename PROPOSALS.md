This page is also available at [gemini://twins.rocketnine.space/proposals.gmi](gemini://twins.rocketnine.space/proposals.gmi)

The following proposals add fields to the response header of successful
requests.

Standard successful response header (for comparison):

```
20 text/gemini; charset=utf-8
```

These additions are backwards-compatible as specified in [RFC 2045](https://tools.ietf.org/html/rfc2045):

> MIME implementations must ignore any parameters whose names they do not recognize.

The terms MAY, SHOULD and SHOULD NOT are defined in [RFC 2119](https://tools.ietf.org/html/rfc2119).

# Cache

Gemini servers MAY include a duration (in seconds) which a client SHOULD cache
a resource when the request is successful. When a cache duration of 0 or
less is provided, clients SHOULD NOT cache the resource.

Cache for one week:

```
20 text/gemini; charset=utf-8; cache=604800
```

Do not cache:

```
20 text/gemini; charset=utf-8; cache=0
```

# Size

Gemini servers SHOULD include the size (in bytes) of the response body when the
request is successful. Clients SHOULD utilize this information when downloading
files to indicate progress.

```
20 text/gemini; charset=utf-8; size=1108
```
