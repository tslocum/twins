package main

import (
	"bytes"
	"net"
	"net/url"
	"os/exec"
	"strings"
)

func serveCommand(c net.Conn, serve *pathConfig, request *url.URL, command []string) (int, int64) {
	var args []string
	if len(command) > 0 {
		args = command[1:]
	}
	cmd := exec.Command(command[0], args...)

	var buf bytes.Buffer
	if request.RawQuery != "" {
		requestQuery, err := url.QueryUnescape(request.RawQuery)
		if err != nil {
			return writeStatus(c, statusBadRequest), -1
		}
		cmd.Stdin = strings.NewReader(requestQuery + "\n")
	}
	cmd.Stdout = &buf
	cmd.Stderr = &buf

	err := cmd.Run()
	if err != nil {
		return writeStatus(c, statusProxyError), -1
	}

	writeSuccess(c, serve, geminiType, int64(buf.Len()))
	c.Write(buf.Bytes())

	return statusSuccess, int64(buf.Len())
}
